CREATE DATABASE drinks
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'pt_BR.utf8'
       LC_CTYPE = 'pt_BR.utf8'
       CONNECTION LIMIT = -1;

CREATE SEQUENCE public.drinks_id_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 8
  CACHE 1;

CREATE TABLE public.drinks
(
  id bigint NOT NULL DEFAULT nextval('drinks_id_seq'::regclass),
  name character varying(255)
);

ALTER TABLE public.drinks ADD COLUMN receiver VARCHAR(255) DEFAULT 'Não definido'