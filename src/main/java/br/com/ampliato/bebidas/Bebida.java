package br.com.ampliato.bebidas;
import br.com.ampliato.fornecedores.Fornecedor;
import br.com.ampliato.produtos.Produtos;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Id;

@RestController
@RequestMapping(value = "Bebidas")
@Entity
@Table(name = "bebidas")
public class Bebida implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @ManyToOne
    @JoinColumn (name = "fornecedores_id")
    private Fornecedor fornecedor;

    @JsonIgnore
    @OneToMany(mappedBy = "produtos_id")
    private List<Produtos> produtos;

    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false)
    private String marca;

    @Column(nullable = false)
    private double volume;

    @Column(nullable = false)
    private String ingredientes;

    @Column(nullable = false)
    private String fabricante;

    @Column(nullable = false)
    private double teorAlcoolico;

    @Column(nullable = false)
    private double peso;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getTeorAlcoolico() {
        return teorAlcoolico;
    }

    public void setTeorAlcoolico(double teorAlcoolico) {
        this.teorAlcoolico = teorAlcoolico;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }


}
