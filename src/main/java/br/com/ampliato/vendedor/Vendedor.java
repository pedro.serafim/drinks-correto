package br.com.ampliato.vendedor;
import br.com.ampliato.pedido.Pedido;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.persistence.Id;
import java.io.Serializable;
import javax.persistence.*;

@RestController
@RequestMapping(value = "Vendedor")
@Entity
@Table(name = "vendedores")
public class Vendedor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    @ManyToOne
    @JoinColumn (name = "pedidos_id")
    private Pedido pedido;

    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String nomeVendedor;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private double telefone;


    public double getTelefone() {
        return telefone;
    }
    public void setTelefone(Double telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getNome() {
        return nomeVendedor;
    }
    public void setNome(String nome) {
        this.nomeVendedor = nome;
    }
}



