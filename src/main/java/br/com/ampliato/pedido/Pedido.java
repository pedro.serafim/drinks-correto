package br.com.ampliato.pedido;
import br.com.ampliato.clientes.Cliente;
import br.com.ampliato.produtos.Produtos;
import br.com.ampliato.vendedor.Vendedor;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Id;

@RestController
@RequestMapping(value = "Pedido")
@Entity
@Table(name = "pedidos")
public class Pedido implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @ManyToOne
    @JoinColumn (name = "clientes_id")
    private Cliente clientes;

    @JsonIgnore
    @OneToMany(mappedBy = "vendedores")
    private List<Vendedor> vendedores;

    @ManyToMany
    @JoinTable(name = "produtos", joinColumns = @JoinColumn(name = "produtos_id"),
            inverseJoinColumns = @JoinColumn(name = "quantidade_id"))
    private List<Produtos> produto = new ArrayList<>();

    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String produtos;

    @Column(nullable = false)
    private double quantidade;

    @Column(nullable = false)
    private String cliente;

    @Column(nullable = false)
    private String enderecoEntrega;

    @Column(nullable = false)
    private Date dataPedido;

    @Column(nullable = false)
    private String status;

    public String getProdutos() {
        return produtos;
    }

    public void setProdutos(String produtos) {
        this.produtos = produtos;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEnderecoEntrega() {
        return enderecoEntrega;
    }

    public void setEnderecoEntrega(String enderecoEntrega) {
        this.enderecoEntrega = enderecoEntrega;
    }

    public Date getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
