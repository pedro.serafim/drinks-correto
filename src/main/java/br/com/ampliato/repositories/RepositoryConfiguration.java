package br.com.ampliato.repositories;

import org.springframework.context.annotation.Configuration;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;


@Configuration
public class RepositoryConfiguration {
    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.postgresql.Driver");
        dataSourceBuilder.url("jdbc:postgresql://localhost:5432/drinks");
        dataSourceBuilder.username("postgres");
        dataSourceBuilder.password("qwpoaslk1209");
        return dataSourceBuilder.build();

    }
    }