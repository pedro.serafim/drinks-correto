package br.com.ampliato.repositories;
import br.com.ampliato.bebidas.Bebida;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DrinksRepository extends CrudRepository<Bebida, Long> {
}
