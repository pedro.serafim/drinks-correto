package br.com.ampliato.repositories;

import br.com.ampliato.produtos.Produtos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdutoRepository extends CrudRepository<Produtos, Long> {
}

