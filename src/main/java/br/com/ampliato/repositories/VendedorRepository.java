package br.com.ampliato.repositories;

import br.com.ampliato.vendedor.Vendedor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

    @Repository
    public interface VendedorRepository extends CrudRepository<Vendedor, Long> {
    }


