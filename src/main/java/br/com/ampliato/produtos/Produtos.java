package br.com.ampliato.produtos;
import br.com.ampliato.bebidas.Bebida;
import br.com.ampliato.pedido.Pedido;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.persistence.Id;
import javax.persistence.*;

@RestController
@RequestMapping(value = "Produtos")
@Entity
public class Produtos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @ManyToOne
    @JoinColumn (name = "bebidas_id")
    private Bebida bebida;

    @ManyToOne
    @JoinColumn (name = "pedidos_id", nullable = false)
    private Pedido pedido;

    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String bebidas;

    @Column(nullable = false)
    private double quantidade;

    public String getBebidas() {
        return bebidas;
    }

    public void setBebidas(String bebidas) {
        this.bebidas = bebidas;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }


}
