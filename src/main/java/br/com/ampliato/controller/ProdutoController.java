package br.com.ampliato.controller;

import br.com.ampliato.fornecedores.Fornecedor;
import br.com.ampliato.produtos.Produtos;
import br.com.ampliato.repositories.FornecedorRepository;
import br.com.ampliato.repositories.ProdutoRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.Assert.notNull;

@RestController
@RequestMapping(value = "produtos")
public class ProdutoController {

    @Autowired
    private ProdutoRepository repository;
    public ProdutoController() {
    }

    @PostMapping
    public Produtos create(@RequestBody Produtos produto) {
        return repository.save(produto);
    }

    @GetMapping("/{id}")
    public Produtos read(@PathVariable Long id) throws NotFoundException {
        Produtos produto = repository.findById(id).orElseThrow(() -> new NotFoundException("produto não encontrado"));
        return produto;
    }

    @PutMapping()
    public Produtos update(@RequestBody Produtos produto) {
        return repository.save(produto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        notNull(id, "id existente");
        repository.deleteById(id);
    }

    @GetMapping("list")
    public List<Produtos> list() {
        List<Produtos> result = new ArrayList<Produtos>();
        repository.findAll().forEach(result::add);

        return result;
    }

}