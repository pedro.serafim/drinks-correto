package br.com.ampliato.controller;

import br.com.ampliato.repositories.VendedorRepository;
import br.com.ampliato.vendedor.Vendedor;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.Assert.notNull;

@RestController
@RequestMapping(value = "vendedores")
public class VendedorController {

    @Autowired
    private VendedorRepository repository;

    public VendedorController() {
        System.out.println("Busca de vendedores");
    }

    @PostMapping
    public Vendedor create(@RequestBody Vendedor vendedor) {
        notNull(vendedor, "Digite o nome do vendedor");
        return repository.save(vendedor);
    }

    @GetMapping("/{id}")
    public Vendedor read(@PathVariable Long id) throws NotFoundException {
        Vendedor vendedor = repository.findById(id).orElseThrow(() -> new NotFoundException("Vendedor não encontrado"));
        return vendedor;
    }

    @PutMapping()
    public Vendedor update(@RequestBody Vendedor vendedor) {
        notNull(vendedor, "");
        return repository.save(vendedor);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        notNull(id, "id presente");
        repository.deleteById(id);
    }

    @GetMapping("list")
    public List<Vendedor> list() {
        List<Vendedor> result = new ArrayList<Vendedor>();
        repository.findAll().forEach(result::add);

        return result;
    }

}
