package br.com.ampliato.controller;

import br.com.ampliato.clientes.Cliente;
import br.com.ampliato.repositories.ClienteRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import static org.springframework.util.Assert.notNull;


@RestController
@RequestMapping(value = "clientes")
public class ClienteController {

    @Autowired
    private ClienteRepository repository;

    public ClienteController() {
        System.out.println("Cliente");
    }

    @PostMapping
    public Cliente create(@RequestBody Cliente cliente) {
        notNull(cliente.getNome(), "Digite o nome do vendedor");
        return repository.save(cliente);
    }

    @GetMapping("/{id}")
    public Cliente read(@PathVariable Long id) throws NotFoundException {
        Cliente cliente = repository.findById(id).orElseThrow(() -> new NotFoundException("Cliente não encontrado"));
        return cliente;
    }

    @PutMapping()
    public Cliente update(@RequestBody Cliente cliente) {
        notNull(cliente, "");
        return repository.save(cliente);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        notNull(id, "Esse id já existe");
        repository.deleteById(id);
    }

    @GetMapping("list")
    public List<Cliente> list() {
        List<Cliente> result = new ArrayList<Cliente>();
        repository.findAll().forEach(result::add);
        return result;
    }

}

