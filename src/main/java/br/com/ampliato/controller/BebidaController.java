package br.com.ampliato.controller;

import br.com.ampliato.bebidas.Bebida;
import br.com.ampliato.repositories.DrinksRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.Assert.notNull;

@RestController
@RequestMapping(value = "bebidas")
public class BebidaController {

    @Autowired private DrinksRepository repository;
    public BebidaController() {
        System.out.println("Iniciando drinks");
    }

    @PostMapping
    public Bebida create(@RequestBody Bebida bebida) {
        notNull(bebida, "diga a bebida desejada");
        return repository.save(bebida);
    }

    @GetMapping("/{id}")
    public Bebida read(@PathVariable Long id) throws NotFoundException {
        Bebida bebida = repository.findById(id).orElseThrow(() -> new NotFoundException("bebida não encontrada"));
        return bebida;
    }

    @PutMapping()
    public Bebida update(@RequestBody Bebida bebida) {
        notNull(bebida, "Envie uma nova bebida");
        return repository.save(bebida);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        notNull(id, "id existente");
        repository.deleteById(id);
    }

    @GetMapping("list")
    public List<Bebida> list() {
        List<Bebida> result = new ArrayList<Bebida>();
        repository.findAll().forEach(result::add);

        return result;
    }

}
