package br.com.ampliato.controller;

import br.com.ampliato.fornecedores.Fornecedor;
import br.com.ampliato.repositories.FornecedorRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.Assert.notNull;

@RestController
@RequestMapping(value = "fornecedores")
public class FornecedorController {

    @Autowired
    private FornecedorRepository repository;
    public FornecedorController() {
    }

    @PostMapping
    public Fornecedor create(@RequestBody Fornecedor fornecedor) {
        return repository.save(fornecedor);
    }

    @GetMapping("/{id}")
    public Fornecedor read(@PathVariable Long id) throws NotFoundException {
        Fornecedor fornecedor = repository.findById(id).orElseThrow(() -> new NotFoundException("fornecedor não encontrado"));
        return fornecedor;
    }

    @PutMapping()
    public Fornecedor update(@RequestBody Fornecedor fornecedor) {
        return repository.save(fornecedor);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        notNull(id, "id existente");
        repository.deleteById(id);
    }

    @GetMapping("list")
    public List<Fornecedor> list() {
        List<Fornecedor> result = new ArrayList<Fornecedor>();
        repository.findAll().forEach(result::add);

        return result;
    }

}