package br.com.ampliato.controller;
import br.com.ampliato.fornecedores.Fornecedor;
import br.com.ampliato.pedido.Pedido;
import br.com.ampliato.repositories.PedidoRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.Assert.notNull;

@RestController
@RequestMapping(value = "pedidos")
public class PedidoController {

    @Autowired
    private PedidoRepository repository;
    public PedidoController() {
    }

    @PostMapping
    public Pedido create(@RequestBody Pedido pedido) {
        return repository.save(pedido);
    }

    @GetMapping("/{id}")
    public Pedido read(@PathVariable Long id) throws NotFoundException {
        Pedido pedido = repository.findById(id).orElseThrow(() -> new NotFoundException("pedido não encontrado"));
        return pedido;
    }

    @PutMapping()
    public Pedido update(@RequestBody Pedido pedido) {
        return repository.save(pedido);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        notNull(id, "id existente");
        repository.deleteById(id);
    }

    @GetMapping("list")
    public List<Pedido> list() {
        List<Pedido> result = new ArrayList<Pedido>();
        repository.findAll().forEach(result::add);

        return result;
    }

}