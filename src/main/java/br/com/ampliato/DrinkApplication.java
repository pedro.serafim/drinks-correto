package br.com.ampliato;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication (scanBasePackages = "br.com.ampliato")
public class DrinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrinkApplication.class,args);
	}

}
